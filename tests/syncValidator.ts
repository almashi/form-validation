import { expect } from 'chai';
import { SyncValidator } from '../src/syncValidator';
import { INPUT_TYPE, emailRegex, phoneNumberRegex, floatingPointNumberRegex } from '../src';

import 'mocha';

const makeCorrectRandomPhoneNumber = () => {
  return Math.random() > 0.5 ? `+${Math.round((Math.random() + 0.09) * 999999999999)}` : `${Math.round((Math.random() + 0.09) * 9999999999)}`;
};

const generateSomeBullshit = () => {
  return Math.random().toString(36).substring(7);
};

const makeCorrectEmail = () => {
  return `${generateSomeBullshit()}@${generateSomeBullshit()}.com`;
};

const makeCorrectFloatingPointNumber = () => {
  return Math.random() > 0.5 ? `${Math.round(Math.random() * 9999)}.${Math.round(Math.random() * 9999)}` : `${Math.round(Math.random() * 9999)}`;
};

describe('SyncValidator', () => {

  // number validation

  it('input number validation required, should fail', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: null,
        },
      },
      undefined,
    );
    expect(result.code).to.equal('INPUT_NUMBER_REQUIRED');
  });

  it('input number validation required, should be ok', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: null,
        },
      },
      12,
    );
    expect(result).to.equal(undefined);
  });

  it('input number validation, should fail', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: '10.1',
          max: '20.2',
          regex: null,
        },
      },
      '10.0',
    );
    expect(result.code).to.equal('INPUT_NUMBER_MIN');
  });

  it('input number validation minimal value, should be ok', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: 122,
          max: null,
          regex: null,
        },
      },
      123,
    );
    expect(result).to.equal(undefined);
  });

  it('input number validation maximal value, should fail', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: 1,
          max: 20,
          regex: null,
        },
      },
      1000,
    );
    expect(result.code).to.equal('INPUT_NUMBER_MAX');
  });

  it('input number validation maximal value, should fail 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const result = SyncValidator.validateInput(
        {
          name: 'number',
          type: INPUT_TYPE.NUMBER,
          validation: {
            required: true,
            min: 1,
            max: 20,
            regex: null,
          },
        },
        1000,
      );
      expect(result.code).to.equal('INPUT_NUMBER_MAX');
    }
  });

  it('input number validation maximal value, should fail', () => {
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.NUMBER,
        validation: {
          required: true,
          min: 1,
          max: 1000,
          regex: null,
        },
      },
      20,
    );
    expect(result).to.equal(undefined);
  });

  // phone number regex

  it('input validation of correct phone number regex format', () => {
    const phoneNumber = makeCorrectRandomPhoneNumber();
    const result = SyncValidator.validateInput(
      {
        name: 'phoneNumber',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: phoneNumberRegex,
            code: 'REGEX_PHONE_NUMBER',
            params: {},
          },
        },
      },
      phoneNumber,
    );
    expect(result).to.equal(undefined);
  });


  it('input validation of correct phone number regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const phoneNumber = makeCorrectRandomPhoneNumber();
      const result = SyncValidator.validateInput(
        {
          name: 'phoneNumber',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: phoneNumberRegex,
              code: 'REGEX_PHONE_NUMBER',
              params: {},
            },
          },
        },
        phoneNumber,
      );
      expect(result).to.equal(undefined);
    }
  });

  it('input validation of bad phone number regex format', () => {
    const phoneNumber = generateSomeBullshit();
    const result = SyncValidator.validateInput(
      {
        name: 'phoneNumber',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: phoneNumberRegex,
            code: 'REGEX_PHONE_NUMBER',
            params: {},
          },
        },
      },
      phoneNumber,
    );
    expect(result.code).to.equal('REGEX_PHONE_NUMBER');
  });


  it('input validation of bad phone number regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const phoneNumber = generateSomeBullshit();
      const result = SyncValidator.validateInput(
        {
          name: 'phoneNumber',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: phoneNumberRegex,
              code: 'REGEX_PHONE_NUMBER',
              params: {},
            },
          },
        },
        phoneNumber,
      );
      expect(result.code).to.equal('REGEX_PHONE_NUMBER');
    }
  });

  it('form validation of correct phone number regex format', () => {
    const phoneNumber = makeCorrectRandomPhoneNumber();
    const result = SyncValidator.validateForm(
      {
        phoneNumber: {
          name: 'phoneNumber',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: phoneNumberRegex,
              code: 'REGEX_PHONE_NUMBER',
              params: {},
            },
          },
        },
      },
      { phoneNumber },
    );
    expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
  });


  it('form validation of correct phone number regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const phoneNumber = makeCorrectRandomPhoneNumber();
      const result = SyncValidator.validateForm(
        {
          phoneNumber: {
            name: 'phoneNumber',
            type: INPUT_TYPE.TEXT,
            validation: {
              required: true,
              min: null,
              max: null,
              regex: {
                expression: phoneNumberRegex,
                code: 'REGEX_PHONE_NUMBER',
                params: {},
              },
            },
          },
        },
        { phoneNumber },
      );
      expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    }
  });

  // email validation regex

  it('input validation of correct email regex format', () => {
    const email = makeCorrectEmail();
    const result = SyncValidator.validateInput(
      {
        name: 'email',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: emailRegex,
            code: 'REGEX_EMAIL',
            params: {},
          },
        },
      },
      email,
    );
    expect(result).to.equal(undefined);
  });


  it('input validation of correct email regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const email = makeCorrectEmail();
      const result = SyncValidator.validateInput(
        {
          name: 'email',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: emailRegex,
              code: 'REGEX_EMAIL',
              params: {},
            },
          },
        },
        email,
      );
      expect(result).to.equal(undefined);
    }
  });

  it('input validation of bad email regex format', () => {
    const email = generateSomeBullshit();
    const result = SyncValidator.validateInput(
      {
        name: 'email',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: emailRegex,
            code: 'REGEX_EMAIL',
            params: {},
          },
        },
      },
      email,
    );
    expect(result.code).to.equal('REGEX_EMAIL');
  });


  it('input validation of bad email regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const email = generateSomeBullshit();
      const result = SyncValidator.validateInput(
        {
          name: 'email',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: emailRegex,
              code: 'REGEX_EMAIL',
              params: {},
            },
          },
        },
        email,
      );
      expect(result.code).to.equal('REGEX_EMAIL');
    }
  });

  it('form validation of correct email regex format', () => {
    const email = makeCorrectEmail();
    const result = SyncValidator.validateForm(
      {
        email: {
          name: 'email',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: emailRegex,
              code: 'REGEX_EMAIL',
              params: {},
            },
          },
        },
      },
      { email },
    );
    expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
  });


  it('form validation of correct email regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const email = makeCorrectEmail();
      const result = SyncValidator.validateForm(
        {
          email: {
            name: 'email',
            type: INPUT_TYPE.TEXT,
            validation: {
              required: true,
              min: null,
              max: null,
              regex: {
                expression: emailRegex,
                code: 'REGEX_EMAIL',
                params: {},
              },
            },
          },
        },
        { email },
      );
      expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    }
  });

  // floating point number validation

  it('input validation of correct floating point number regex format', () => {
    const number = makeCorrectFloatingPointNumber();
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: floatingPointNumberRegex,
            code: 'REGEX_FLOATING_POINT_NUMBER',
            params: {},
          },
        },
      },
      number,
    );
    expect(result).to.equal(undefined);
  });


  it('input validation of correct floating point number regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const number = makeCorrectFloatingPointNumber();
      const result = SyncValidator.validateInput(
        {
          name: 'number',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: floatingPointNumberRegex,
              code: 'REGEX_FLOATING_POINT_NUMBER',
              params: {},
            },
          },
        },
        number,
      );
      expect(result).to.equal(undefined);
    }
  });

  it('input validation of bad  floating point number regex format', () => {
    const number = generateSomeBullshit();
    const result = SyncValidator.validateInput(
      {
        name: 'number',
        type: INPUT_TYPE.TEXT,
        validation: {
          required: true,
          min: null,
          max: null,
          regex: {
            expression: floatingPointNumberRegex,
            code: 'REGEX_FLOATING_POINT_NUMBER',
            params: {},
          },
        },
      },
      number,
    );
    expect(result.code).to.equal('REGEX_FLOATING_POINT_NUMBER');
  });


  it('input validation of bad email regex format 100 times', () => {
    for (let i = 0; i < 100; i++) {
      const number = generateSomeBullshit();
      const result = SyncValidator.validateInput(
        {
          name: 'number',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: floatingPointNumberRegex,
              code: 'REGEX_FLOATING_POINT_NUMBER',
              params: {},
            },
          },
        },
        number,
      );
      expect(result.code).to.equal('REGEX_FLOATING_POINT_NUMBER');
    }
  });

  it('form validation of correct floating point number regex format', () => {
    const number = makeCorrectFloatingPointNumber();
    const result = SyncValidator.validateForm(
      {
        number: {
          name: 'number',
          type: INPUT_TYPE.TEXT,
          validation: {
            required: true,
            min: null,
            max: null,
            regex: {
              expression: floatingPointNumberRegex,
              code: 'REGEX_FLOATING_POINT_NUMBER',
              params: {},
            },
          },
        },
      },
      { number },
    );
    expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
  });


  it('form validation of correct floating point number regex format', () => {
    for (let i = 0; i < 100; i++) {
      const number = makeCorrectFloatingPointNumber();
      const result = SyncValidator.validateForm(
        {
          number: {
            name: 'number',
            type: INPUT_TYPE.TEXT,
            validation: {
              required: true,
              min: null,
              max: null,
              regex: {
                expression: floatingPointNumberRegex,
                code: 'REGEX_FLOATING_POINT_NUMBER',
                params: {},
              },
            },
          },
        },
        { number },
      );
      expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    }
  });


});
