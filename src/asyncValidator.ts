import { validationInterface, inputInterface, formInterface, inputErrorInterface } from './interfaces';
import { INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, parameterPrefix as errorParameterPrefix, parameterPostfix as errorParameterPostfix } from './enums';
import * as Bluebird from 'bluebird';

const floatingPointNumberRegex = /^[+-]?\d+(\.\d+)?$/;

class Validator {

  public static validateForm(form: formInterface, values: any): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        const validationBluebirds = Object.keys(form).map((key) => {
          return this.validateInput(form[key], values[key]);
        });
        const validations = await Bluebird.all(validationBluebirds.map(((Bluebird) => Bluebird.catch((error) => { return error; }))));
        const errors = validations.reduce((errors, value) => {
          if (value !== undefined) {
            errors[value.name] = { code: value.code, params: value.params };
          }
          return errors;
        }, {});
        if (Object.keys(errors).length > 0) {
          return reject(errors);
        }
        return resolve();
      } catch (err) {
        return reject(err);
      }
    })
  }

  public static errorMessage(message: string, params: any): Bluebird<string> {
    return new Bluebird((resolve, reject) => {
      try {
        let errorMessage = message;
        Object.keys(params).map((key) => {
          errorMessage = errorMessage.split(`${errorParameterPrefix}${key}${errorParameterPostfix}`).join(params[key]);
        });
        return resolve(errorMessage);
      } catch (err) {
        return reject(err);
      }
    })
  }

  public static validateInput(input: inputInterface, value: any): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        switch (input.type) {
          case INPUT_TYPE.TEXT:
            await this.validateInputText(input, value);
            break;
          case INPUT_TYPE.NUMBER:
            await this.validateInputNumber(input, value);
            break;
          case INPUT_TYPE.SELECT:
            await this.validateInputSelect(input, value);
            break;
          case INPUT_TYPE.MULTISELECT:
            await this.validateInputMultiSelect(input, value);
            break;
          case INPUT_TYPE.CHECKBOX:
            await this.validateInputCheckbox(input, value);
            break;
          case INPUT_TYPE.RADIO:
            await this.validateInputRadio(input, value);
            break;
          case INPUT_TYPE.DATE:
            await this.validateInputDate(input, value);
            break;
          default:
            return resolve(); // reject(new Error(INPUT_TYPE_VALIDATION_ERROR.UNKNOWN));
        }
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputDate(input: inputInterface, value: string): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.checkInputDateRequired(input.validation, input.name, value);
        await this.checkInputDateMin(input.validation, input.name, value);
        await this.checkInputDateMax(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputDateRequired(validation: validationInterface, name: string, value: any): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputDateMin(validation: validationInterface, name: string, value: any): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.min !== null && new Date(validation.min) > new Date(value) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MIN,
          params: { min: validation.min },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputDateMax(validation: validationInterface, name: string, value: any): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.max !== null && new Date(validation.max) < new Date(value) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MAX,
          params: { max: validation.max },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }


  private static validateInputText(input: inputInterface, value: string): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.checkInputTextRequired(input.validation, input.name, value);
        await this.checkInputTextMin(input.validation, input.name, value);
        await this.checkInputTextMax(input.validation, input.name, value);
        await this.checkInputTextRegex(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputTextRequired(validation: validationInterface, name: string, value: string): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputTextMin(validation: validationInterface, name: string, value: string): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.min !== null && validation.min > value.length ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MIN,
          params: { min: validation.min },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputTextMax(validation: validationInterface, name: string, value: string): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.max !== null && validation.max < value.length ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MAX,
          params: { max: validation.max },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputTextRegex(validation: validationInterface, name: string, value: string): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        if (validation.regex !== null) {
          const regex = validation.regex.expression.constructor === RegExp ? validation.regex.expression : new RegExp(validation.regex.expression);
          if (!regex.test(value)) {
            reject(<inputErrorInterface>{
              name,
              code: validation.regex.code,
              params: { ...validation.regex.params },
            })
          }
        }
        resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputNumber(input: inputInterface, value: number): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.checkInputNumberRequired(input.validation, input.name, value);
        await this.checkInputNumberRegex(input.validation, input.name, value);
        await this.checkInputNumberMin(input.validation, input.name, value);
        await this.checkInputNumberMax(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputNumberRequired(validation: validationInterface, name: string, value: number): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputNumberMin(validation: validationInterface, name: string, value: any): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.min !== null && parseFloat(validation.min) > parseFloat(value) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MIN,
          params: { min: validation.min },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputNumberMax(validation: validationInterface, name: string, value: any): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.max !== null && parseFloat(validation.max) < parseFloat(value) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
          params: { max: validation.max },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputNumberRegex(validation: validationInterface, name: string, value: any) {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        const regex = new RegExp(floatingPointNumberRegex);
        !regex.test(value) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
          params: { max: validation.max },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputSelect(input: inputInterface, value: number): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.checkInputSelectRequired(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputSelectRequired(validation: validationInterface, name: string, value: number): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_SELECT_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputMultiSelect(input: inputInterface, value: Array<number>): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.checkInputMultiSelectRequired(input.validation, input.name, value);
        await this.checkInputMultiSelectMin(input.validation, input.name, value);
        await this.checkInputMultiSelectMax(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputMultiSelectRequired(validation: validationInterface, name: string, value: Array<number>): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && (!value || value.length === 0) ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputMultiSelectMin(validation: validationInterface, name: string, value: Array<number>): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.min !== null && validation.min > value.length ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MIN,
          params: { min: validation.min },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static checkInputMultiSelectMax(validation: validationInterface, name: string, value: Array<number>): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        !validation.required && !value && resolve();
        return validation.max !== null && validation.max < value.length ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MAX,
          params: { max: validation.max },
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputCheckbox(input: inputInterface, value: boolean): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.validateInputCheckboxRequired(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputCheckboxRequired(validation: validationInterface, name: string, value: boolean): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_CHECKBOX_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputRadio(input: inputInterface, value: string): Bluebird<void> {
    return new Bluebird(async (resolve, reject) => {
      try {
        await this.validateInputRadioRequired(input.validation, input.name, value);
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

  private static validateInputRadioRequired(validation: validationInterface, name: string, value: string): Bluebird<void> {
    return new Bluebird((resolve, reject) => {
      try {
        return validation.required && !value ? reject(<inputErrorInterface>{
          name,
          code: INPUT_TYPE_VALIDATION_ERROR.INPUT_RADIO_REQUIRED,
          params: {},
        }) : resolve();
      } catch (err) {
        return reject(err);
      }
    });
  }

}

export { Validator };
