import { Validator } from './asyncValidator';
import { SyncValidator } from './syncValidator';
import { INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, parameterPrefix as errorParameterPrefix, parameterPostfix as errorParameterPostfix } from './enums';

const phoneNumberRegex = /^[0-9\+]{9,15}$/;
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const floatingPointNumberRegex = /^[+-]?\d+(\.\d+)?$/;

export { Validator, SyncValidator, INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, errorParameterPrefix, errorParameterPostfix, phoneNumberRegex, emailRegex, floatingPointNumberRegex };