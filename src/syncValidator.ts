import { validationInterface, inputInterface, formInterface, inputErrorInterface } from './interfaces';
import { INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, parameterPrefix as errorParameterPrefix, parameterPostfix as errorParameterPostfix } from './enums';
import * as Bluebird from 'bluebird';

const floatingPointNumberRegex = /^[+-]?\d+(\.\d+)?$/;

class SyncValidator {

  public static validateForm(form: formInterface, values: any) {
    const validations = Object.keys(form).map((key) => {
      return this.validateInput(form[key], values[key]);
    });
    return validations.reduce((errors, value) => {
      if (value !== undefined) {
        errors[value.name] = { code: value.code, params: value.params };
      }
      return errors;
    }, {});
  }

  public static validateInput(input: inputInterface, value: any) {
    switch (input.type) {
      case INPUT_TYPE.TEXT:
        return this.validateInputText(input, value);
      case INPUT_TYPE.NUMBER:
        return this.validateInputNumber(input, value);
      case INPUT_TYPE.SELECT:
        return this.validateInputSelect(input, value);
      case INPUT_TYPE.MULTISELECT:
        return this.validateInputMultiSelect(input, value);
      case INPUT_TYPE.CHECKBOX:
        return this.validateInputCheckbox(input, value);
      case INPUT_TYPE.RADIO:
        return this.validateInputRadio(input, value);
      case INPUT_TYPE.DATE:
        return this.validateInputDate(input, value);
      default:
        return;
    }
  }

  private static validateInputDate(input: inputInterface, value: any) {
    const required = this.checkInputDateRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    const min = this.checkInputDateMin(input.validation, input.name, value);
    if (min) {
      return min;
    }
    const max = this.checkInputDateMax(input.validation, input.name, value);
    if (max) {
      return max;
    }
    return;
  }


  private static checkInputDateRequired(validation: validationInterface, name: string, value: any): Bluebird<void> {
    if (validation.required && !value)
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_REQUIRED,
        params: {},
      };
    return;
  }

  private static checkInputDateMin(validation: validationInterface, name: string, value: any): Bluebird<void> {
    if (!validation.required && !value)
      return;
    if (validation.min !== null && new Date(validation.min) > new Date(value))
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MIN,
        params: { min: validation.min },
      };
    return;
  }

  private static checkInputDateMax(validation: validationInterface, name: string, value: any): Bluebird<void> {
    if (!validation.required && !value)
      return;
    if (validation.max !== null && new Date(validation.max) < new Date(value))
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MAX,
        params: { max: validation.max },
      };
    return;
  }

  public static errorMessage(message: string, params: any) {
    let errorMessage = message;
    Object.keys(params).map((key) => {
      errorMessage = errorMessage.split(`${errorParameterPrefix}${key}${errorParameterPostfix}`).join(params[key]);
    });
    return errorMessage;
  }

  private static validateInputText(input: inputInterface, value: string) {
    const required = this.checkInputTextRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    const min = this.checkInputTextMin(input.validation, input.name, value);
    if (min) {
      return min;
    }
    const max = this.checkInputTextMax(input.validation, input.name, value);
    if (max) {
      return max;
    }
    const regex = this.checkInputTextRegex(input.validation, input.name, value);
    if (regex) {
      return regex;
    }
    return;
  }

  private static checkInputTextRequired(validation: validationInterface, name: string, value: string) {
    if (validation.required && value.length === 0) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_REQUIRED,
        params: {},
      };
    }
    return;
  }

  private static checkInputTextMin(validation: validationInterface, name: string, value: string) {
    if (!validation.required && !value)
      return;
    if (validation.min !== null && validation.min > value.length) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MIN,
        params: { min: validation.min },
      };
    }
    return;
  }

  private static checkInputTextMax(validation: validationInterface, name: string, value: string) {
    if (!validation.required && !value)
      return;
    if (validation.max !== null && validation.max < value.length) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MAX,
        params: { max: validation.max },
      };
    }
    return;
  }

  private static checkInputTextRegex(validation: validationInterface, name: string, value: string) {
    if (!validation.required && !value)
      return undefined;
    if (validation.regex !== null) {
      const regex = new RegExp(validation.regex.expression);
      if (!regex.test(value)) {
        return <inputErrorInterface>{
          name,
          code: validation.regex.code,
          params: { ...validation.regex.params },
        };
      }
    }
    return undefined;
  }

  private static validateInputNumber(input: inputInterface, value: number) {
    const required = this.checkInputNumberRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    const regex = this.checkInputNumberRegex(input.validation, input.name, value);
    if (regex) {
      return regex;
    }
    const min = this.checkInputNumberMin(input.validation, input.name, value);
    if (min) {
      return min;
    }
    const max = this.checkInputNumberMax(input.validation, input.name, value);
    if (max) {
      return max;
    }
    return;
  }

  private static checkInputNumberRequired(validation: validationInterface, name: string, value: number) {
    if (validation.required && !value) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_REQUIRED,
        params: {},
      };
    }
    return;
  }

  private static checkInputNumberMin(validation: validationInterface, name: string, value: any) {
    if (!validation.required && !value)
      return;
    if (validation.min !== null && parseFloat(validation.min) > parseFloat(value)) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MIN,
        params: { min: validation.min },
      };
    }
    return;
  }

  private static checkInputNumberMax(validation: validationInterface, name: string, value: any) {
    if (!validation.required && !value)
      return;
    if (validation.max !== null && parseFloat(validation.max) < parseFloat(value)) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
        params: { max: validation.max },
      };
    }
    return;
  }

  private static checkInputNumberRegex(validation: validationInterface, name: string, value: any) {
    if (!validation.required && !value)
      return undefined;
    const regex = new RegExp(floatingPointNumberRegex);
    if (!regex.test(value)) {
      return <inputErrorInterface>{
        name,
        code: 'REGEX_NUMBER',
        params: {},
      };
    }
  }

  private static validateInputSelect(input: inputInterface, value: number) {
    const required = this.checkInputSelectRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    return;
  }

  private static checkInputSelectRequired(validation: validationInterface, name: string, value: number) {
    if (validation.required && !value) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_SELECT_REQUIRED,
        params: {},
      };
    }
    return;
  }

  private static validateInputMultiSelect(input: inputInterface, value: Array<number>) {
    const required = this.checkInputMultiSelectRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    const min = this.checkInputMultiSelectMin(input.validation, input.name, value);
    if (min) {
      return min;
    }
    const max = this.checkInputMultiSelectMax(input.validation, input.name, value);
    if (max) {
      return max;
    }
    return;
  }

  private static checkInputMultiSelectRequired(validation: validationInterface, name: string, value: Array<number>) {
    if (validation.required && (!value || value.length === 0)) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_REQUIRED,
        params: {},
      };
    }
    return;
  }

  private static checkInputMultiSelectMin(validation: validationInterface, name: string, value: Array<number>) {
    if (!validation.required && !value)
      return;
    if (validation.min !== null && validation.min > value.length) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MIN,
        params: { min: validation.min },
      };
    }
    return;
  }

  private static checkInputMultiSelectMax(validation: validationInterface, name: string, value: Array<number>) {
    if (!validation.required && !value)
      return;
    if (validation.max !== null && validation.max < value.length) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MAX,
        params: { max: validation.max },
      };
    }
    return;
  }

  private static validateInputCheckbox(input: inputInterface, value: boolean) {
    const required = this.validateInputCheckboxRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    return;
  }

  private static validateInputCheckboxRequired(validation: validationInterface, name: string, value: boolean) {
    if (validation.required && !value) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_CHECKBOX_REQUIRED,
        params: {},
      };
    }
    return;
  }

  private static validateInputRadio(input: inputInterface, value: string) {
    const required = this.validateInputRadioRequired(input.validation, input.name, value);
    if (required) {
      return required;
    }
    return;
  }

  private static validateInputRadioRequired(validation: validationInterface, name: string, value: string): Bluebird<void> {
    if (validation.required && !value) {
      return <inputErrorInterface>{
        name,
        code: INPUT_TYPE_VALIDATION_ERROR.INPUT_RADIO_REQUIRED,
        params: {},
      };
    }
    return;
  }

}

export { SyncValidator };
