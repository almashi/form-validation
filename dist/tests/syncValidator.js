"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var syncValidator_1 = require("../src/syncValidator");
var src_1 = require("../src");
require("mocha");
var makeCorrectRandomPhoneNumber = function () {
    return Math.random() > 0.5 ? "+" + Math.round((Math.random() + 0.09) * 999999999999) : "" + Math.round((Math.random() + 0.09) * 9999999999);
};
var generateSomeBullshit = function () {
    return Math.random().toString(36).substring(7);
};
var makeCorrectEmail = function () {
    return generateSomeBullshit() + "@" + generateSomeBullshit() + ".com";
};
var makeCorrectFloatingPointNumber = function () {
    return Math.random() > 0.5 ? Math.round(Math.random() * 9999) + "." + Math.round(Math.random() * 9999) : "" + Math.round(Math.random() * 9999);
};
describe('SyncValidator', function () {
    // number validation
    it('input number validation required, should fail', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: null,
            },
        }, undefined);
        chai_1.expect(result.code).to.equal('INPUT_NUMBER_REQUIRED');
    });
    it('input number validation required, should be ok', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: null,
            },
        }, 12);
        chai_1.expect(result).to.equal(undefined);
    });
    it('input number validation, should fail', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: '10.1',
                max: '20.2',
                regex: null,
            },
        }, '10.0');
        chai_1.expect(result.code).to.equal('INPUT_NUMBER_MIN');
    });
    it('input number validation minimal value, should be ok', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: 122,
                max: null,
                regex: null,
            },
        }, 123);
        chai_1.expect(result).to.equal(undefined);
    });
    it('input number validation maximal value, should fail', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: 1,
                max: 20,
                regex: null,
            },
        }, 1000);
        chai_1.expect(result.code).to.equal('INPUT_NUMBER_MAX');
    });
    it('input number validation maximal value, should fail 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'number',
                type: src_1.INPUT_TYPE.NUMBER,
                validation: {
                    required: true,
                    min: 1,
                    max: 20,
                    regex: null,
                },
            }, 1000);
            chai_1.expect(result.code).to.equal('INPUT_NUMBER_MAX');
        }
    });
    it('input number validation maximal value, should fail', function () {
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.NUMBER,
            validation: {
                required: true,
                min: 1,
                max: 1000,
                regex: null,
            },
        }, 20);
        chai_1.expect(result).to.equal(undefined);
    });
    // phone number regex
    it('input validation of correct phone number regex format', function () {
        var phoneNumber = makeCorrectRandomPhoneNumber();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'phoneNumber',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.phoneNumberRegex,
                    code: 'REGEX_PHONE_NUMBER',
                    params: {},
                },
            },
        }, phoneNumber);
        chai_1.expect(result).to.equal(undefined);
    });
    it('input validation of correct phone number regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var phoneNumber = makeCorrectRandomPhoneNumber();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'phoneNumber',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.phoneNumberRegex,
                        code: 'REGEX_PHONE_NUMBER',
                        params: {},
                    },
                },
            }, phoneNumber);
            chai_1.expect(result).to.equal(undefined);
        }
    });
    it('input validation of bad phone number regex format', function () {
        var phoneNumber = generateSomeBullshit();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'phoneNumber',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.phoneNumberRegex,
                    code: 'REGEX_PHONE_NUMBER',
                    params: {},
                },
            },
        }, phoneNumber);
        chai_1.expect(result.code).to.equal('REGEX_PHONE_NUMBER');
    });
    it('input validation of bad phone number regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var phoneNumber = generateSomeBullshit();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'phoneNumber',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.phoneNumberRegex,
                        code: 'REGEX_PHONE_NUMBER',
                        params: {},
                    },
                },
            }, phoneNumber);
            chai_1.expect(result.code).to.equal('REGEX_PHONE_NUMBER');
        }
    });
    it('form validation of correct phone number regex format', function () {
        var phoneNumber = makeCorrectRandomPhoneNumber();
        var result = syncValidator_1.SyncValidator.validateForm({
            phoneNumber: {
                name: 'phoneNumber',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.phoneNumberRegex,
                        code: 'REGEX_PHONE_NUMBER',
                        params: {},
                    },
                },
            },
        }, { phoneNumber: phoneNumber });
        chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    });
    it('form validation of correct phone number regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var phoneNumber = makeCorrectRandomPhoneNumber();
            var result = syncValidator_1.SyncValidator.validateForm({
                phoneNumber: {
                    name: 'phoneNumber',
                    type: src_1.INPUT_TYPE.TEXT,
                    validation: {
                        required: true,
                        min: null,
                        max: null,
                        regex: {
                            expression: src_1.phoneNumberRegex,
                            code: 'REGEX_PHONE_NUMBER',
                            params: {},
                        },
                    },
                },
            }, { phoneNumber: phoneNumber });
            chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
        }
    });
    // email validation regex
    it('input validation of correct email regex format', function () {
        var email = makeCorrectEmail();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'email',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.emailRegex,
                    code: 'REGEX_EMAIL',
                    params: {},
                },
            },
        }, email);
        chai_1.expect(result).to.equal(undefined);
    });
    it('input validation of correct email regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var email = makeCorrectEmail();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'email',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.emailRegex,
                        code: 'REGEX_EMAIL',
                        params: {},
                    },
                },
            }, email);
            chai_1.expect(result).to.equal(undefined);
        }
    });
    it('input validation of bad email regex format', function () {
        var email = generateSomeBullshit();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'email',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.emailRegex,
                    code: 'REGEX_EMAIL',
                    params: {},
                },
            },
        }, email);
        chai_1.expect(result.code).to.equal('REGEX_EMAIL');
    });
    it('input validation of bad email regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var email = generateSomeBullshit();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'email',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.emailRegex,
                        code: 'REGEX_EMAIL',
                        params: {},
                    },
                },
            }, email);
            chai_1.expect(result.code).to.equal('REGEX_EMAIL');
        }
    });
    it('form validation of correct email regex format', function () {
        var email = makeCorrectEmail();
        var result = syncValidator_1.SyncValidator.validateForm({
            email: {
                name: 'email',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.emailRegex,
                        code: 'REGEX_EMAIL',
                        params: {},
                    },
                },
            },
        }, { email: email });
        chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    });
    it('form validation of correct email regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var email = makeCorrectEmail();
            var result = syncValidator_1.SyncValidator.validateForm({
                email: {
                    name: 'email',
                    type: src_1.INPUT_TYPE.TEXT,
                    validation: {
                        required: true,
                        min: null,
                        max: null,
                        regex: {
                            expression: src_1.emailRegex,
                            code: 'REGEX_EMAIL',
                            params: {},
                        },
                    },
                },
            }, { email: email });
            chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
        }
    });
    // floating point number validation
    it('input validation of correct floating point number regex format', function () {
        var number = makeCorrectFloatingPointNumber();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.floatingPointNumberRegex,
                    code: 'REGEX_FLOATING_POINT_NUMBER',
                    params: {},
                },
            },
        }, number);
        chai_1.expect(result).to.equal(undefined);
    });
    it('input validation of correct floating point number regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var number = makeCorrectFloatingPointNumber();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'number',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.floatingPointNumberRegex,
                        code: 'REGEX_FLOATING_POINT_NUMBER',
                        params: {},
                    },
                },
            }, number);
            chai_1.expect(result).to.equal(undefined);
        }
    });
    it('input validation of bad  floating point number regex format', function () {
        var number = generateSomeBullshit();
        var result = syncValidator_1.SyncValidator.validateInput({
            name: 'number',
            type: src_1.INPUT_TYPE.TEXT,
            validation: {
                required: true,
                min: null,
                max: null,
                regex: {
                    expression: src_1.floatingPointNumberRegex,
                    code: 'REGEX_FLOATING_POINT_NUMBER',
                    params: {},
                },
            },
        }, number);
        chai_1.expect(result.code).to.equal('REGEX_FLOATING_POINT_NUMBER');
    });
    it('input validation of bad email regex format 100 times', function () {
        for (var i = 0; i < 100; i++) {
            var number = generateSomeBullshit();
            var result = syncValidator_1.SyncValidator.validateInput({
                name: 'number',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.floatingPointNumberRegex,
                        code: 'REGEX_FLOATING_POINT_NUMBER',
                        params: {},
                    },
                },
            }, number);
            chai_1.expect(result.code).to.equal('REGEX_FLOATING_POINT_NUMBER');
        }
    });
    it('form validation of correct floating point number regex format', function () {
        var number = makeCorrectFloatingPointNumber();
        var result = syncValidator_1.SyncValidator.validateForm({
            number: {
                name: 'number',
                type: src_1.INPUT_TYPE.TEXT,
                validation: {
                    required: true,
                    min: null,
                    max: null,
                    regex: {
                        expression: src_1.floatingPointNumberRegex,
                        code: 'REGEX_FLOATING_POINT_NUMBER',
                        params: {},
                    },
                },
            },
        }, { number: number });
        chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
    });
    it('form validation of correct floating point number regex format', function () {
        for (var i = 0; i < 100; i++) {
            var number = makeCorrectFloatingPointNumber();
            var result = syncValidator_1.SyncValidator.validateForm({
                number: {
                    name: 'number',
                    type: src_1.INPUT_TYPE.TEXT,
                    validation: {
                        required: true,
                        min: null,
                        max: null,
                        regex: {
                            expression: src_1.floatingPointNumberRegex,
                            code: 'REGEX_FLOATING_POINT_NUMBER',
                            params: {},
                        },
                    },
                },
            }, { number: number });
            chai_1.expect(JSON.stringify(result)).to.equal(JSON.stringify({}));
        }
    });
});
