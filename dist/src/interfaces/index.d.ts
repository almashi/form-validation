declare interface String {
  replaceAll(search: string, replacement: string): string;
}

interface regexInterface {
  expression: any;
  code: string;
  params: any;
}

interface validationInterface {
  required: boolean;
  min: any;
  max: any;
  regex?: regexInterface
}

interface inputInterface {
  name: string;
  type: string;
  validation: validationInterface;
}

interface formInterface {
  [key: string]: inputInterface;
}

interface inputErrorInterface {
  name: string;
  code: string;
  params: any;
}

export { regexInterface, validationInterface, inputInterface, formInterface, inputErrorInterface, String };