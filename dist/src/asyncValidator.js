"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = require("./enums");
var Bluebird = require("bluebird");
var floatingPointNumberRegex = /^[+-]?\d+(\.\d+)?$/;
var Validator = /** @class */ (function () {
    function Validator() {
    }
    Validator.validateForm = function (form, values) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var validationBluebirds, validations, errors, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        validationBluebirds = Object.keys(form).map(function (key) {
                            return _this.validateInput(form[key], values[key]);
                        });
                        return [4 /*yield*/, Bluebird.all(validationBluebirds.map((function (Bluebird) { return Bluebird.catch(function (error) { return error; }); })))];
                    case 1:
                        validations = _a.sent();
                        errors = validations.reduce(function (errors, value) {
                            if (value !== undefined) {
                                errors[value.name] = { code: value.code, params: value.params };
                            }
                            return errors;
                        }, {});
                        if (Object.keys(errors).length > 0) {
                            return [2 /*return*/, reject(errors)];
                        }
                        return [2 /*return*/, resolve()];
                    case 2:
                        err_1 = _a.sent();
                        return [2 /*return*/, reject(err_1)];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.errorMessage = function (message, params) {
        return new Bluebird(function (resolve, reject) {
            try {
                var errorMessage_1 = message;
                Object.keys(params).map(function (key) {
                    errorMessage_1 = errorMessage_1.split("" + enums_1.parameterPrefix + key + enums_1.parameterPostfix).join(params[key]);
                });
                return resolve(errorMessage_1);
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInput = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var _a, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 17, , 18]);
                        _a = input.type;
                        switch (_a) {
                            case enums_1.INPUT_TYPE.TEXT: return [3 /*break*/, 1];
                            case enums_1.INPUT_TYPE.NUMBER: return [3 /*break*/, 3];
                            case enums_1.INPUT_TYPE.SELECT: return [3 /*break*/, 5];
                            case enums_1.INPUT_TYPE.MULTISELECT: return [3 /*break*/, 7];
                            case enums_1.INPUT_TYPE.CHECKBOX: return [3 /*break*/, 9];
                            case enums_1.INPUT_TYPE.RADIO: return [3 /*break*/, 11];
                            case enums_1.INPUT_TYPE.DATE: return [3 /*break*/, 13];
                        }
                        return [3 /*break*/, 15];
                    case 1: return [4 /*yield*/, this.validateInputText(input, value)];
                    case 2:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 3: return [4 /*yield*/, this.validateInputNumber(input, value)];
                    case 4:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 5: return [4 /*yield*/, this.validateInputSelect(input, value)];
                    case 6:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 7: return [4 /*yield*/, this.validateInputMultiSelect(input, value)];
                    case 8:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 9: return [4 /*yield*/, this.validateInputCheckbox(input, value)];
                    case 10:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 11: return [4 /*yield*/, this.validateInputRadio(input, value)];
                    case 12:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 13: return [4 /*yield*/, this.validateInputDate(input, value)];
                    case 14:
                        _b.sent();
                        return [3 /*break*/, 16];
                    case 15: return [2 /*return*/, resolve()]; // reject(new Error(INPUT_TYPE_VALIDATION_ERROR.UNKNOWN));
                    case 16: return [2 /*return*/, resolve()];
                    case 17:
                        err_2 = _b.sent();
                        return [2 /*return*/, reject(err_2)];
                    case 18: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.validateInputDate = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.checkInputDateRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputDateMin(input.validation, input.name, value)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputDateMax(input.validation, input.name, value)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 4:
                        err_3 = _a.sent();
                        return [2 /*return*/, reject(err_3)];
                    case 5: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.checkInputDateRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputDateMin = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.min !== null && new Date(validation.min) > new Date(value) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MIN,
                    params: { min: validation.min },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputDateMax = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.max !== null && new Date(validation.max) < new Date(value) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MAX,
                    params: { max: validation.max },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputText = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, this.checkInputTextRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputTextMin(input.validation, input.name, value)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputTextMax(input.validation, input.name, value)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputTextRegex(input.validation, input.name, value)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 5:
                        err_4 = _a.sent();
                        return [2 /*return*/, reject(err_4)];
                    case 6: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.checkInputTextRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputTextMin = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.min !== null && validation.min > value.length ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MIN,
                    params: { min: validation.min },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputTextMax = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.max !== null && validation.max < value.length ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MAX,
                    params: { max: validation.max },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputTextRegex = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                if (validation.regex !== null) {
                    var regex = validation.regex.expression.constructor === RegExp ? validation.regex.expression : new RegExp(validation.regex.expression);
                    if (!regex.test(value)) {
                        reject({
                            name: name,
                            code: validation.regex.code,
                            params: __assign({}, validation.regex.params),
                        });
                    }
                }
                resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputNumber = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, this.checkInputNumberRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputNumberRegex(input.validation, input.name, value)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputNumberMin(input.validation, input.name, value)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputNumberMax(input.validation, input.name, value)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 5:
                        err_5 = _a.sent();
                        return [2 /*return*/, reject(err_5)];
                    case 6: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.checkInputNumberRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputNumberMin = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.min !== null && parseFloat(validation.min) > parseFloat(value) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MIN,
                    params: { min: validation.min },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputNumberMax = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.max !== null && parseFloat(validation.max) < parseFloat(value) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
                    params: { max: validation.max },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputNumberRegex = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                var regex = new RegExp(floatingPointNumberRegex);
                !regex.test(value) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
                    params: { max: validation.max },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputSelect = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.checkInputSelectRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 2:
                        err_6 = _a.sent();
                        return [2 /*return*/, reject(err_6)];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.checkInputSelectRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_SELECT_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputMultiSelect = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        return [4 /*yield*/, this.checkInputMultiSelectRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputMultiSelectMin(input.validation, input.name, value)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.checkInputMultiSelectMax(input.validation, input.name, value)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 4:
                        err_7 = _a.sent();
                        return [2 /*return*/, reject(err_7)];
                    case 5: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.checkInputMultiSelectRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && (!value || value.length === 0) ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputMultiSelectMin = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.min !== null && validation.min > value.length ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MIN,
                    params: { min: validation.min },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.checkInputMultiSelectMax = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                !validation.required && !value && resolve();
                return validation.max !== null && validation.max < value.length ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MAX,
                    params: { max: validation.max },
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputCheckbox = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.validateInputCheckboxRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 2:
                        err_8 = _a.sent();
                        return [2 /*return*/, reject(err_8)];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.validateInputCheckboxRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_CHECKBOX_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    Validator.validateInputRadio = function (input, value) {
        var _this = this;
        return new Bluebird(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var err_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.validateInputRadioRequired(input.validation, input.name, value)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, resolve()];
                    case 2:
                        err_9 = _a.sent();
                        return [2 /*return*/, reject(err_9)];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    Validator.validateInputRadioRequired = function (validation, name, value) {
        return new Bluebird(function (resolve, reject) {
            try {
                return validation.required && !value ? reject({
                    name: name,
                    code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_RADIO_REQUIRED,
                    params: {},
                }) : resolve();
            }
            catch (err) {
                return reject(err);
            }
        });
    };
    return Validator;
}());
exports.Validator = Validator;
