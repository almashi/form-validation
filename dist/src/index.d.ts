import { Validator } from './asyncValidator';
import { SyncValidator } from './syncValidator';
import { INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, parameterPrefix as errorParameterPrefix, parameterPostfix as errorParameterPostfix } from './enums';
declare const phoneNumberRegex: RegExp;
declare const emailRegex: RegExp;
declare const floatingPointNumberRegex: RegExp;
export { Validator, SyncValidator, INPUT_TYPE, INPUT_TYPE_VALIDATION_ERROR, errorParameterPrefix, errorParameterPostfix, phoneNumberRegex, emailRegex, floatingPointNumberRegex };
