"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = require("./enums");
var floatingPointNumberRegex = /^[+-]?\d+(\.\d+)?$/;
var SyncValidator = /** @class */ (function () {
    function SyncValidator() {
    }
    SyncValidator.validateForm = function (form, values) {
        var _this = this;
        var validations = Object.keys(form).map(function (key) {
            return _this.validateInput(form[key], values[key]);
        });
        return validations.reduce(function (errors, value) {
            if (value !== undefined) {
                errors[value.name] = { code: value.code, params: value.params };
            }
            return errors;
        }, {});
    };
    SyncValidator.validateInput = function (input, value) {
        switch (input.type) {
            case enums_1.INPUT_TYPE.TEXT:
                return this.validateInputText(input, value);
            case enums_1.INPUT_TYPE.NUMBER:
                return this.validateInputNumber(input, value);
            case enums_1.INPUT_TYPE.SELECT:
                return this.validateInputSelect(input, value);
            case enums_1.INPUT_TYPE.MULTISELECT:
                return this.validateInputMultiSelect(input, value);
            case enums_1.INPUT_TYPE.CHECKBOX:
                return this.validateInputCheckbox(input, value);
            case enums_1.INPUT_TYPE.RADIO:
                return this.validateInputRadio(input, value);
            case enums_1.INPUT_TYPE.DATE:
                return this.validateInputDate(input, value);
            default:
                return;
        }
    };
    SyncValidator.validateInputDate = function (input, value) {
        var required = this.checkInputDateRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        var min = this.checkInputDateMin(input.validation, input.name, value);
        if (min) {
            return min;
        }
        var max = this.checkInputDateMax(input.validation, input.name, value);
        if (max) {
            return max;
        }
        return;
    };
    SyncValidator.checkInputDateRequired = function (validation, name, value) {
        if (validation.required && !value)
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_REQUIRED,
                params: {},
            };
        return;
    };
    SyncValidator.checkInputDateMin = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.min !== null && new Date(validation.min) > new Date(value))
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MIN,
                params: { min: validation.min },
            };
        return;
    };
    SyncValidator.checkInputDateMax = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.max !== null && new Date(validation.max) < new Date(value))
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_DATE_MAX,
                params: { max: validation.max },
            };
        return;
    };
    SyncValidator.errorMessage = function (message, params) {
        var errorMessage = message;
        Object.keys(params).map(function (key) {
            errorMessage = errorMessage.split("" + enums_1.parameterPrefix + key + enums_1.parameterPostfix).join(params[key]);
        });
        return errorMessage;
    };
    SyncValidator.validateInputText = function (input, value) {
        var required = this.checkInputTextRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        var min = this.checkInputTextMin(input.validation, input.name, value);
        if (min) {
            return min;
        }
        var max = this.checkInputTextMax(input.validation, input.name, value);
        if (max) {
            return max;
        }
        var regex = this.checkInputTextRegex(input.validation, input.name, value);
        if (regex) {
            return regex;
        }
        return;
    };
    SyncValidator.checkInputTextRequired = function (validation, name, value) {
        if (validation.required && value.length === 0) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_REQUIRED,
                params: {},
            };
        }
        return;
    };
    SyncValidator.checkInputTextMin = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.min !== null && validation.min > value.length) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MIN,
                params: { min: validation.min },
            };
        }
        return;
    };
    SyncValidator.checkInputTextMax = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.max !== null && validation.max < value.length) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_TEXT_MAX,
                params: { max: validation.max },
            };
        }
        return;
    };
    SyncValidator.checkInputTextRegex = function (validation, name, value) {
        if (!validation.required && !value)
            return undefined;
        if (validation.regex !== null) {
            var regex = new RegExp(validation.regex.expression);
            if (!regex.test(value)) {
                return {
                    name: name,
                    code: validation.regex.code,
                    params: __assign({}, validation.regex.params),
                };
            }
        }
        return undefined;
    };
    SyncValidator.validateInputNumber = function (input, value) {
        var required = this.checkInputNumberRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        var regex = this.checkInputNumberRegex(input.validation, input.name, value);
        if (regex) {
            return regex;
        }
        var min = this.checkInputNumberMin(input.validation, input.name, value);
        if (min) {
            return min;
        }
        var max = this.checkInputNumberMax(input.validation, input.name, value);
        if (max) {
            return max;
        }
        return;
    };
    SyncValidator.checkInputNumberRequired = function (validation, name, value) {
        if (validation.required && !value) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_REQUIRED,
                params: {},
            };
        }
        return;
    };
    SyncValidator.checkInputNumberMin = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.min !== null && parseFloat(validation.min) > parseFloat(value)) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MIN,
                params: { min: validation.min },
            };
        }
        return;
    };
    SyncValidator.checkInputNumberMax = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.max !== null && parseFloat(validation.max) < parseFloat(value)) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_NUMBER_MAX,
                params: { max: validation.max },
            };
        }
        return;
    };
    SyncValidator.checkInputNumberRegex = function (validation, name, value) {
        if (!validation.required && !value)
            return undefined;
        var regex = new RegExp(floatingPointNumberRegex);
        if (!regex.test(value)) {
            return {
                name: name,
                code: 'REGEX_NUMBER',
                params: {},
            };
        }
    };
    SyncValidator.validateInputSelect = function (input, value) {
        var required = this.checkInputSelectRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        return;
    };
    SyncValidator.checkInputSelectRequired = function (validation, name, value) {
        if (validation.required && !value) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_SELECT_REQUIRED,
                params: {},
            };
        }
        return;
    };
    SyncValidator.validateInputMultiSelect = function (input, value) {
        var required = this.checkInputMultiSelectRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        var min = this.checkInputMultiSelectMin(input.validation, input.name, value);
        if (min) {
            return min;
        }
        var max = this.checkInputMultiSelectMax(input.validation, input.name, value);
        if (max) {
            return max;
        }
        return;
    };
    SyncValidator.checkInputMultiSelectRequired = function (validation, name, value) {
        if (validation.required && (!value || value.length === 0)) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_REQUIRED,
                params: {},
            };
        }
        return;
    };
    SyncValidator.checkInputMultiSelectMin = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.min !== null && validation.min > value.length) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MIN,
                params: { min: validation.min },
            };
        }
        return;
    };
    SyncValidator.checkInputMultiSelectMax = function (validation, name, value) {
        if (!validation.required && !value)
            return;
        if (validation.max !== null && validation.max < value.length) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_MULTISELECT_MAX,
                params: { max: validation.max },
            };
        }
        return;
    };
    SyncValidator.validateInputCheckbox = function (input, value) {
        var required = this.validateInputCheckboxRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        return;
    };
    SyncValidator.validateInputCheckboxRequired = function (validation, name, value) {
        if (validation.required && !value) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_CHECKBOX_REQUIRED,
                params: {},
            };
        }
        return;
    };
    SyncValidator.validateInputRadio = function (input, value) {
        var required = this.validateInputRadioRequired(input.validation, input.name, value);
        if (required) {
            return required;
        }
        return;
    };
    SyncValidator.validateInputRadioRequired = function (validation, name, value) {
        if (validation.required && !value) {
            return {
                name: name,
                code: enums_1.INPUT_TYPE_VALIDATION_ERROR.INPUT_RADIO_REQUIRED,
                params: {},
            };
        }
        return;
    };
    return SyncValidator;
}());
exports.SyncValidator = SyncValidator;
